import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./components/Login";
import Registrarse from "./components/Registrarse";
import Navbar from "./components/Navbar";
import Home from "./components/Home";
import Materia from "./components/Materia";
import MateriaList from "./components/MateriaList";
import Laboratorio from "./components/Laboratorio";
import LaboratorioList from "./components/LaboratorioList";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/registrate" element={<Registrarse />} />
          <Route path="/home" element={<Home />} />
          <Route path="/materia/new" element={<Materia />} />
          <Route path="/materia" element={<MateriaList />} />
          <Route path="/materia/:id_materia/edit" element={<Materia />} />
          <Route path="/lab/new" element={<Laboratorio />} />
          <Route path="/lab" element={<LaboratorioList />} />
          <Route path="/lab/:id_laboratorio/edit" element={<Laboratorio />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
