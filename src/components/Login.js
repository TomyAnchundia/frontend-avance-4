import React from "react";
import "./css/login.css";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const handleSubmit = async (e) => {
    e.preventDefault();
    const res = await fetch("http://localhost:4000/login", {
      method: "POST",
      body: JSON.stringify(valida),
      headers: { "Content-Type": "application/json" },
    });
    const data = await res.json();

    if (data === "usuario no existe") {
      alert("intentelo de nuevo");
    } else if (data === "usuario existe") {
      alert("usuario logueado con exito");
      navigate("/home");
    }
  };

  const [valida, validacion] = useState({
    nombre_usuario: "",
    contrasena_usuario: "",
  });

  const navigate = useNavigate();

  const handleChange = (e) => {
    validacion({ ...valida, [e.target.name]: e.target.value });
  };

  return (
    <div className="container">
      <h1> Iniciar Sesión</h1>
      <form onSubmit={handleSubmit}>
        <div className="date">
          <input
            type="text"
            name="nombre_usuario"
            onChange={handleChange}
            required
          />
          <span></span>
          <label>Nombre de Usuario</label>
        </div>
        <div className="date">
          <input
            type="password"
            name="contrasena_usuario"
            onChange={handleChange}
            required
            autoComplete="on"
          />
          <span></span>
          <label>Contraseña</label>
        </div>
        <div className="forgot-pass"> olvido su contraseña?</div>
        <input className="btn-login" type="submit" value="Iniciar sesión" />
        <div className="link-registrarse">
          No estas registrado?{" "}
          <a className="enlace-registrate" href="/registrate">
            {" "}
            Registrate
          </a>
        </div>
      </form>
    </div>
  );
};

export default Login;
