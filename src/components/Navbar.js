import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import "./css/styles.css";
import logo from "../images/FacciLOGO.png";

const Navbar = () => {
  const navigate = useNavigate();
  const [logged, setLogged] = useState(true);
  const handleLogin = () => {
      setLogged(true);
      navigate('/home')
  }
  const handleLogout = () => {
    setLogged(false);
    navigate("/");
  };

  return (
    <header className="nav">
      <nav className="navbar">
        <a href={"#"}>
          {" "}
          <img className="logo-nav" src={logo} />{" "}
        </a>
        <a className="nav-gest-lab" href={"#"}>
          {" "}
          <h5>GESTIÓN DE LABORATORIOS</h5>{" "}
        </a>
        <ul className="nav-menu"></ul>

        <div className="options-nav">
          {logged ? (
            <button className="btn-logout-nav" onClick={handleLogout}>
              Logout
            </button>
          ) : (
            <Link to="/home">
              <button className="btn-login-nav" onClick ={handleLogin} >Login</button>
            </Link>
          )}
        </div>
      </nav>
    </header>
  );
};

export default Navbar;
